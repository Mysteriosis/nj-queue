import BodyParser from 'koa-bodyparser'
import Koa from 'koa'
import Logger from 'koa-logger'
import Passport from 'koa-passport'
import Pug from 'koa-pug'
import Sentry from '@sentry/node'
import ServeStatic from 'koa-static'
import Session from 'koa-session'
import SocketIO from 'koa-socket-2'
import Momentjs from 'moment'

// import auth from './config/auth.mjs'
import config from './config/config.mjs'
import router from './config/routes.mjs'

const app = new Koa();

// Init middlewares

Sentry.init({ dsn: 'https://071a358a0e314becbc1035a56e481a11@sentry.io/1545336' });
app.use(Logger());
app.use(BodyParser());
app.use(router.routes());
app.use(ServeStatic('./views/assets'))

// Auth
// app.keys = [config.auth.secret]
// app.use(Session({}, app))
// app.use(Passport.initialize())
// app.use(Passport.session())
// auth()

// Socket.io part
const socketIO = new SocketIO()
socketIO.attach(app);
socketIO.on('connection', function(socket) {
   console.log('User connected');
})

const pug = new Pug({
   viewPath: './views',
   basedir: './views',
   noCache: config.app.env === 'development',
   locals: {
      moment: Momentjs
   },
   app: app
})

// Error handler in production
app.use(async (ctx, next) => {
   if(config.app.env === 'production') {
      try {
         await next();
         if ((ctx.status || 404) === 404)
            ctx.throw(404)
      } catch (err) {
         ctx.status = err.status || 500;
         ctx.body = err.message;
         ctx.app.emit('error', err, ctx);
      }
   }
})

// Event displaying error
app.on('error', (err, ctx) => {
   console.log("Error", err)
   console.log("Context", ctx)

   Sentry.withScope(scope => {
      scope.addEventProcessor(event => Sentry.Handlers.parseRequest(event, ctx.request))
      Sentry.captureException(err)
   })

   ctx.render('error.pug', {
      title: (ctx.status === 404) ? 'Page not found' : 'Something went wrong',
      status: (ctx.status === 404) ? 404 : err.status,
      message: err.message
   })
})

// Start server
app.listen(config.app.port);