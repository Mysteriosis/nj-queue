import Queue from '../models/Queue.mjs'
import Controller from './controller.mjs'

class homeController extends Controller {
    async index(ctx) {
        ctx.redirect('/queues')
    }
}

export default new homeController()