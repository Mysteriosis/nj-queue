import Nodemailer from 'nodemailer'
import QRCode from 'qrcode'
import Soap from 'soap'

import User from '../models/User.mjs'
import Queue from '../models/Queue.mjs'
import Visitor from '../models/Visitor.mjs'
import Station from '../models/Station.mjs'

import Controller from './controller.mjs'

class queueController extends Controller {
    async generateQR(text) {
        try {
            return await QRCode.toDataURL(text)
        } catch (err) {
            console.error(err)
        }
    }

    async index(ctx) {
        try {
            const queuesList = await Queue.find({});

            ctx.render('queues/index.pug', {
                title: "List of queues",
                queuesList: queuesList
            })
        }
        catch (err) {
            throw err;
        }
    }

    async show(ctx) {
        const queue = await Queue.findOne({_id: ctx.params.queue_id})
            .populate({
                path: 'stations',
                populate: {
                    path: 'visitor',
                    model: 'Visitor'
                }
            })
            .populate({
                path:'visitors',
                populate: {
                    path: 'station',
                    model: 'Station'
                }
            })
            .sort('numId')

        if('rawData' in ctx.query) {
            ctx.body = JSON.stringify(queue)
        }
        else {
            const qr = await this.generateQR(ctx.request.origin + "/queues/" + queue._id + "/visitors")

            ctx.render('queues/show.pug', {
                hostname: ctx.request.origin,
                infosQueue: queue,
                qrCode: qr
            })
        }
    }

    async edit(ctx) {
        const queue = await Queue.findOne({_id: ctx.params.queue_id})
        // TODO: Add visitor
    }

    ////////////////////////////
    // Manage stations in queues
    ////////////////////////////

    async stations(ctx) {
        // TODO: Display warning if not in opening hours
        const queue = await Queue.findOne({_id: ctx.params.queue_id})
            .populate({
                path: 'stations',
                populate: {
                    path: 'visitor',
                    model: 'Visitor'
                }
            })
            .populate('visitors')

        const qr = await this.generateQR(ctx.request.origin + "/queues/" + queue._id + "/visitors")

        ctx.render('queues/stations.pug', {
            infosQueue: queue,
            qrCode: qr
        })
    }

    ////////////////////////////
    // Manage visitors in queues
    ////////////////////////////

    async visitors(ctx) {
        // TODO: Display warning if not in opening hours
        const queue = await Queue.findOne({_id: ctx.params.queue_id})
            .populate('visitors')
            .populate('stations')

        const waitingVisitors = queue.visitors.filter(v => v.status === 'waiting')
        const openStations = queue.stations.filter(v => v.status)
        const wait = (waitingVisitors.length * 8) / openStations.length
        const hours = Math.floor(wait/60)
        const minutes = Math.floor(wait%60)

        ctx.render('queues/visitors.pug', {
            infosQueue: queue,
            waitingTime: hours + ":" + minutes
        })
    }

    async createVisitor(ctx, config) {
        let phoneOk = true
        let mailOk = true
        let phoneCheck = false
        let mailCheck = false

        let phone = null
        let email = null

        // Check mobile phone
        if(!!ctx.request.body.phone) {
            phone = ctx.request.body.phone
                .replace(/^(\+41|0041)/, "")
                .replace(/[^0-9]/g,"")

            if(phone.length === 9 && phone[0] !== "0") {
                phone = "0" + phone
            }

            if(phone.length !== 10 || isNaN(Number(phone))) {
                phoneOk = false
            }
        }
        else {
            phoneOk = false
        }

        // Check mail
        if(!!ctx.request.body.email) {
            email = ctx.request.body.email

            if(!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email))) {
                mailOk = false
            }
        }
        else {
            mailOk = false
        }

        if(phoneOk || mailOk) {
            const queue = await Queue.findOne({_id: ctx.params.queue_id})
                .populate('visitors')

            queue.visitors.forEach(function(visitor) {
                if(visitor.phone === phone || visitor.email === email) {
                    throw `You are already referenced as number ${visitor.numId} in this queue !`
                }
            });

            // Create new Visitor
            const newVisitor = new Visitor({
                numId: queue.actualNumber + 1,
                phone: phone || '',
                email: email || '',
                infos: {
                    logs: []
                }
            })

            queue.visitors.push(newVisitor)
            queue.actualNumber += 1
            queue.save()

            newVisitor.logs.push(Controller.LOG_CREATE())
            await newVisitor.save()

            const textMessage = `Vous avez été ajouté avec succès dans la file d'attente "${queue.name}".
Votre numéro de passage est le : ${newVisitor.numId}`
                    // Vous pouvez consulter la file d'attente à l'adresse: ${ctx.request.origin}/queues/${ctx.params.queue_id}`

            const HTMLMessage = `Vous avez été ajouté avec succès dans la file d'attente <b>"${queue.name}"</b>.<br />
                    Votre numéro de passage est le : <b>${newVisitor.numId}</b><br />
                    Vous pouvez consulter la file d'attente à l'adresse: <a href="${ctx.request.origin}/queues/${ctx.params.queue_id}">${ctx.request.origin}/queues/${ctx.params.queue_id}</a>`

            if(phoneOk) {
                // Complete example of SOAP 1.2 request
                // const message = {
                //     AccountName: config.services.sms.user, // required
                //     AccountPassword: config.services.sms.password, // required
                //     Address: '', // required
                //     Message: '', // required
                //     JobID: '',
                //     SMSCallback: '',
                //     Notification: '',
                //     Answer: '',
                //     SendDate: '',
                //     MsgType: '',
                //     NoLog: '',
                //     AlwaysNotification: ''
                // }

                const soapMsg = {
                    AccountName: config.services.sms.user,
                    AccountPassword: config.services.sms.password,
                    Address: phone,
                    Message: textMessage,
                    SMSCallback: 'Temple VR',
                    JobID: newVisitor.infos.smsJobID
                }

                // Create SOAP client & send SMS
                Soap.createClient(config.services.sms.url + '?WSDL', function(clientError, client) {
                    client.SendSMSBasic(soapMsg, function(queryError, result) {
                        console.log(result)
                        if(!queryError) {
                            newVisitor.logs.push(Controller.LOG_SMS_CREATION(String(Date.now())))
                            newVisitor.save()
                        }
                        else {
                            throw queryError
                        }
                    })
                })
            }

            if(mailOk) {
                const transporter = Nodemailer.createTransport({
                    host: config.services.mail.host,
                    port: config.services.mail.port,
                    secure: true, // true for 465, false for other ports
                    auth: {
                        user: config.services.mail.user,
                        pass: config.services.mail.password
                    }
                })

                await transporter.sendMail({
                    from: '"Temple VR" <noreply@festigeek.ch>', // sender address
                    to: email, // list of receivers
                    subject: `Votre inscription à la file de "${queue.name}"`, // Subject line
                    text: textMessage,
                    html: HTMLMessage
                }).then(infos => {
                    console.log('Message sent: %s', infos.messageId);

                    mailCheck = true
                    queue.visitors.push(newVisitor)
                    queue.actualNumber += 1
                    queue.save()

                    newVisitor.logs.push(Controller.LOG_CREATE())
                    newVisitor.save()
                }).catch(err => {
                    console.log(err)
                    if(!err) {
                        throw "Error when sending mail"
                    }
                })
            }

            ctx.app.io.broadcast('updateQueue', {})
            ctx.render('queues/createVisitor.pug', {
                infosQueue: queue,
                infoVisitor: newVisitor
            })
        }
        else {
            throw "No suitable phone or email provided"
        }
    }
}

export default new queueController()