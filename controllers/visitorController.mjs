import Controller from './controller.mjs'
import Visitor from '../models/Visitor.mjs'

class visitorController extends Controller {
    async index(ctx) {
        const visitors = Visitor.find({}).sort('numId')

        ctx.render('visitors/index.pug', {
            visitors: visitors
        })
    }

    async show(ctx){
        // ctx.app.visitor.findOne({"_id": ObjectID(ctx.params.id)});
    }
}

export default new visitorController(Visitor)