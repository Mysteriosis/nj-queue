export default class Controller {
    static LOG_CREATE() {
        return { name: "creation", details: `Instance created`, date: Date.now() }
    }
    static LOG_UPDATE() {
        return { name: "update", details: `Instance updated`, date: Date.now() }
    }
    static LOG_DELETE() {
        return { name: "delete", details: `Instance deleted`, date: Date.now() }
    }
    static LOG_MOVE(target) {
        return { name: "move", details: `Instance moved in ${target}`, date: Date.now() }
    }
    static LOG_SMS_CREATION(idJob) {
        return { name: "sms_creation", details: 'Creation SMS sent', smsJobID: idJob, date: Date.now() }
    }
    static LOG_SMS_REMINDER(idJob) {
        return { name: "sms_reminder", details: 'Reminder SMS sent', smsJobID: idJob, date: Date.now() }
    }

    constructor(model) {
        this.model = (model !== undefined) ? model : null;
    }

    index(req) {
        return (!!this.model) ? this.model.find({}) : []
    }

    save(req) {

    }

    delete(req) {

    }
}
