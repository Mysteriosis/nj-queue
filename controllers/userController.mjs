import Controller from './controller.mjs'
import Passport from "koa-passport";

class userController extends Controller {
    async index(ctx) {
        
    }

    async login(ctx) {
        Passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/?login'
        })
    }

    async logout(ctx) {
        ctx.logout()
        ctx.redirect('/')
    }
}

export default new userController()