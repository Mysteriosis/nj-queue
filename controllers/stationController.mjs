import Controller from './controller.mjs'
import Station from '../models/Station.mjs'
import Queue from '../models/Queue.mjs'
import Visitor from '../models/Visitor.mjs'
import Soap from 'soap';

class stationController extends Controller {
    async index(ctx) {
        try {
            const stations = await Station.find({});

            ctx.render('stations/index.pug', {
                stations: stations
            })
        }
        catch (err) {
            throw err;
        }
    }

    async edit(ctx) {
        const station = await Station.findOne({_id: ctx.params.station_id})
        const status = ctx.request.body.status

        if(status === 'true' || status === 'false') {
            station.status = status
            await station.save()
        }

        ctx.app.io.broadcast('updateQueue', {})
        ctx.app.io.broadcast('updateStations', {})
        ctx.redirect('/stations')
    }

    async addVisitor(ctx) {
        const station = await Station.findOne({_id: ctx.params.station_id})
            .populate('visitor')

        const newVisitor = await Visitor.findOne({_id: ctx.params.visitor_id})

        if(!station.visitor) {
            newVisitor.logs.push(Controller.LOG_MOVE(station.name))
            newVisitor.status = 'playing'
            newVisitor.station = station._id
            await newVisitor.save()
            station.visitor = newVisitor._id
            await station.save()

            ctx.app.io.broadcast('updateQueue', {})
            ctx.app.io.broadcast('updateStations', {})
            ctx.render('stations/newVisitor.pug', {
                infoVisitor: newVisitor,
                infoStation: station,
                queue_id: station.queue
            })
        }

        ctx.redirect('/queues/' + station.queue)
    }

    async removeVisitor(ctx, config) {
        const target = ctx.query.target
        const station = await Station.findOne({_id: ctx.params.station_id})
        const visitor = await Visitor.findOne({_id: station.visitor})
        const queue = await Queue.findOne({_id: station.queue})
            .populate('visitors')
            .populate('stations')

        if(visitor.status === "playing") {
            station.visitor = null

            switch(target) {
                case 'passed':
                    visitor.logs.push(Controller.LOG_MOVE('Passed'))
                    visitor.status = 'passed'
                    visitor.station = null
                    break;
                case 'finished':
                    visitor.logs.push(Controller.LOG_MOVE('Finished'))
                    visitor.status = 'finished'
                    visitor.station = null
                    queue.visitors.remove(visitor._id)
                    await queue.save()
                    break;
                default:
                    throw "Unknown target action"
            }

            await visitor.save()
            await station.save()

            const smsRange = queue.stations
                .filter(s => s.status).length
            const smsList = queue.visitors
                .filter(v => (v.status === 'waiting' && !!v.phone))
                .sort((v1, v2) => v1.numId - v2.numId)
                .slice(0, smsRange)

            for(const v of smsList) {
                let send = true

                for(const l of v.logs) {
                    if(l.name === 'sms_reminder') {
                        send = false
                    }
                }

                if(send) {
                    const contactVisitor = await Visitor.findOne({_id: v._id})
                    const textMessage = `Ce sera bientôt à votre tour de passer !
N'oubliez pas de vous présenter à notre stand :)
Votre numéro de passage est le : ${contactVisitor.numId}`

                    const soapMsg = {
                        AccountName: config.services.sms.user,
                        AccountPassword: config.services.sms.password,
                        Address: contactVisitor.phone,
                        Message: textMessage,
                        SMSCallback: 'Temple VR',
                        JobID: contactVisitor.infos.smsJobID
                    }

                    // Create SOAP client & send SMS
                    Soap.createClient(config.services.sms.url + '?WSDL', function(clientError, client) {
                        client.SendSMSBasic(soapMsg, function(queryError, result) {
                            console.log(result)
                            if(!queryError) {
                                contactVisitor.logs.push(Controller.LOG_SMS_REMINDER(Date.now()))
                                contactVisitor.save()
                            }
                        })
                    })
                }
            }

            ctx.app.io.broadcast('updateQueue', {})
            ctx.app.io.broadcast('updateStations', {})
            ctx.redirect('/queues/' + station.queue._id)
        }
    }
}

export default new stationController()