# NJ-Queue
A waiting queue server giving e-tickets to people who want to participate to an event, allowing them to not wait on the event place. This app is designed to work with a native mobile app to deal with notifications.

## Dependencies
- MongoDB
- Node v.11

## Installation
- `git clone https://gitlab.com/Mysteriosis/nj-queue.git && cd nj-queue`
- `yarn install`
- `cp .env.exemple .env (edit as needed)`
- `yarn seed`
- `yarn start`