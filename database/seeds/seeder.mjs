import assert from 'assert'

import User from '../../models/User.mjs'
import Visitor from '../../models/Visitor.mjs'
import Station from '../../models/Station.mjs'
import Queue from '../../models/Queue.mjs'

// DATA
const users = [
    { "_id" : "5c65ed0b3580a510ccc7e005", "roles" : [ "admin", "user" ], "email" : "pacurty@gmail.com", "username" : "Iosis555", "password" : "sokolov" }
]

const visitors = [
    { "_id" : "c705ed15850a5e0b35c600cc", "numId": 1, "status": "playing", "queue": "5c65ed0b3580a510ccc7e006", "station": "7e58c00d1650c5e0ab3c505c", "phone": "0000000", "email": "", "logs": [ { "name": "sms_creation", "details": 'Creation SMS sent', "smsJobID": "1567181772591", "date": "1567181772591" } ],  },
    { "_id" : "5d630182230bba0ec0dc9cdd", "numId": 2, "status": "waiting", "queue": "5c65ed0b3580a510ccc7e006", "station": null, "phone": "" , "email": "pa@curty.tech", "logs": [] },
    { "_id" : "3ec0da05d602219cc8dbbd30", "numId": 3, "status": "waiting", "queue": "5c65ed0b3580a510ccc7e006", "station": null, "phone": "" , "email": "pacurty@gmail.com", "logs": [] }
]

const stations = [
    { "_id" : "7e58c00d1650c5e0ab3c505c", "name": "Poste 1", "queue": "5c65ed0b3580a510ccc7e006", "visitor": "c705ed15850a5e0b35c600cc", "status": true, "infos": {"img_enabled": "../../images/vive_ok.png", "img_disabled": "../../images/vive_off.png"} },
    { "_id" : "d1c08050c57e5cc5e650b30a", "name": "Poste 2", "queue": "5c65ed0b3580a510ccc7e006", "visitor": null, "status": true, "infos": {"img_enabled": "../../images/vive_ok.png", "img_disabled": "../../images/vive_off.png"} },
    { "_id" : "00cc5e6557e58ce505c0ab3c", "name": "Poste 3", "queue": "5c65ed0b3580a510ccc7e006", "visitor": null, "status": true, "infos": {"img_enabled": "../../images/vive_ok.png", "img_disabled": "../../images/vive_off.png"} },
    { "_id" : "800d16505b30ad1c0c50c57e", "name": "Poste 4", "queue": "5c65ed0b3580a510ccc7e006", "visitor": null, "status": true, "infos": {"img_enabled": "../../images/vive_ok.png", "img_disabled": "../../images/vive_off.png"} }
]

const queues = [
    {   "_id" : "5c65ed0b3580a510ccc7e006",
        "name" : "Temple VR",
        "author": "5c65ed0b3580a510ccc7e005",
        "actualNumber": 3,
        "infos": {},
        "schedule": {
            "friday": {
                "startAt": "16:30:00",
                "endAt": "23:45:00"
            },
            "saturday": {
                "startAt": "13:30:00",
                "endAt": "23:45:00"
            },
            "sunday": {
                "startAt": "11:00:00",
                "endAt": "18:00:00"
            },
        },
        "visitors" : [
            "c705ed15850a5e0b35c600cc",
            "5d630182230bba0ec0dc9cdd",
            "3ec0da05d602219cc8dbbd30"
        ],
        "stations": [
            "7e58c00d1650c5e0ab3c505c",
            "d1c08050c57e5cc5e650b30a",
            "00cc5e6557e58ce505c0ab3c",
            "800d16505b30ad1c0c50c57e"
        ]
    }
]

// SAVE
const actions = [
    User.deleteMany({}, function(err) {
        console.log('Collection User removed')
    }),
    User.insertMany(users, { rawResult: true }, function(err, r) {
        assert.strictEqual(null, err)
        assert.strictEqual(users.length, r.insertedCount)
        User.db.close()
    }),

    Visitor.deleteMany({}, function(err) {
        console.log('Collection Visitor removed')
    }),
    Visitor.insertMany(visitors, { rawResult: true }, function(err, r) {
        assert.strictEqual(null, err)
        assert.strictEqual(visitors.length, r.insertedCount)
        Visitor.db.close()
    }),

    Station.deleteMany({}, function(err) {
        console.log('Collection Station removed')
    }),
    Station.insertMany(stations, { rawResult: true }, function(err, r) {
        assert.strictEqual(null, err)
        assert.strictEqual(stations.length, r.insertedCount)
        Station.db.close()
    }),

    Queue.deleteMany({}, function(err) {
        console.log('Collection Queue removed')
    }),
    Queue.insertMany(queues, { rawResult: true }, function(err, r) {
        assert.strictEqual(null, err)
        assert.strictEqual(queues.length, r.insertedCount)
        Queue.db.close()
    })
]

Promise.all(actions, function() {
    process.exit()
})