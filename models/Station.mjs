import mongoose from 'mongoose'

const stationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    queue: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Queue',
        required: true
    },
    visitor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Visitor',
        required: false
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    infos: {
        type: Object,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    deleted_at: {
        type: Date,
        default: null,
        required: false
    }
});

export default mongoose.model('Station', stationSchema);