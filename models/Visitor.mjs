import mongoose from 'mongoose'

const visitorSchema = new mongoose.Schema({
    numId: {
        type: Number,
        required: true,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        enum: ['waiting', 'playing', 'passed', 'finished'],
        default: 'waiting'
    },
    station: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Station',
        required: false
    },
    queue: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Queue',
        required: false
    },
    infos: {
        type: Object,
        required: false
    },
    logs: {
        type: [Object],
        default: [],
        required: false
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    deleted_at: {
        type: Date,
        default: null,
        required: false
    }
});

export default mongoose.model('Visitor', visitorSchema);