import mongoose from '../config/database.mjs'
import bcrypt from 'bcryptjs'

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        set: pwd => bcrypt.hashSync(pwd, 10)
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    roles: {
        type: [String],
        lowercase: true,
        default: ['user']
    },
    provider: {
        type: String,
        lowercase: true,
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    deleted_at: {
        type: Date,
        default: null,
        required: false
    }
})

userSchema.methods.comparePass = async function(userPassword) {
    return bcrypt.compareSync(userPassword, this.password);
}

export default mongoose.model('User', userSchema)