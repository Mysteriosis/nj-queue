import mongoose from 'mongoose'

const queueSchema = new mongoose.Schema({
    name: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    actualNumber: {
        type: Number,
        required: true,
        default: 0
    },
    infos: {
        type: Object,
        required: false
    },
    schedule: {
        type: Object,
        required: false
    },
    visitors: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Visitor'
        }
    ],
    stations: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Station'
        }
    ],
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    deleted_at: {
        type: Date,
        default: null,
        required: false
    }
});

export default mongoose.model('Queue', queueSchema);