import Passport from 'koa-passport'
import LocalStrategy from 'passport-local'

import User from '../models/User.mjs'


const auth = function (app) {
    Passport.serializeUser( (user, done) => done(null, user._id) )

    Passport.deserializeUser( (id, done) => {
        return User.findOne({_id: id}).exec( (err, user) => {
            (err) ? done(err, null) : done(null, user)
        })
    });

    Passport.use(new LocalStrategy( (username, password, done) => {
        return  User.findOne({ username: username.toLowerCase() }).exec(async (err, user) => {
            if (err) {
                return done(err)
            }

            if (!user) {
                return done(undefined, false, { message: `User ${username} not found.` })
            }

            return (await user.comparePass(password)) ? done(undefined, user) : done(undefined, false, { message: "Invalid username or password." })
        })
    }))
}

export default auth