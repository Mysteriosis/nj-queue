// import Passport from 'koa-passport'
import Router from 'koa-router'

import config from './config.mjs'

// Controllers
import homeController from '../controllers/homeController.mjs'
// import userController from '../controllers/userController.mjs'
import queueController from '../controllers/queueController.mjs'
import stationController from '../controllers/stationController.mjs'
import visitorController from '../controllers/visitorController.mjs'

const router = new Router()

/**
 * Home page : Display home page
 * @return ./views/home.pug
 */
router.get("/", async (ctx) => {
    await homeController.index(ctx)
})

/////////////
// QUEUES
/////////////

/**
 * Queues : List of active queues
 *
 * @return ./views/queues/index.pug
 */
router.get("/queues/", async (ctx) => {
    await queueController.index(ctx)
})

/**
 * Queues : List of visitors in queue
 *
 * @return ./views/queues/index.pug
 */
router.get("/queues/:queue_id", async (ctx) => {
    await queueController.show(ctx)
})

/**
 * Queues : Page to display add visitor's number form
 *
 * @return ./views/queues/add.pug
 */
router.get("/queues/:queue_id/visitors", async (ctx) => {
    await queueController.visitors(ctx)
})

/**
 * Queues : Page to add visitor's number
 *
 * @return ./views/queues/add.pug
 */
router.post("/queues/:queue_id/visitors", async (ctx) => {
    await queueController.createVisitor(ctx, config)
})

/**
 * Queues : Page to display available stations for the queue
 *
 * @return ./views/queues/stations.pug
 */
router.get("/queues/:queue_id/stations", async (ctx) => {
    await queueController.stations(ctx, config)
})

// // Update one
// router.put("/visitor/:id", async (ctx) => {
//     let documentQuery = {"_id": ObjectID(ctx.params.id)};
//     let valuesToUpdate = ctx.request.body;
//     ctx.body = await ctx.app.visitor.updateOne(documentQuery, valuesToUpdate);
// });

// // Delete one
// router.delete("/visitor/:id", async (ctx) => {
//     let documentQuery = {"_id": ObjectID(ctx.params.id)};
//     ctx.body = await ctx.app.visitor.deleteOne(documentQuery);
// });


/////////////
// STATIONS
/////////////

/**
 * Queues : List of stations
 *
 * @return ./views/stations/index.pug
 */
router.get("/stations/", async (ctx) => {
    await stationController.index(ctx)
})

/**
 * Queues : Edit a station
 *
 * @return ./views/stations/edit.pug
 */
router.post("/stations/:station_id", async (ctx) => {
    await stationController.edit(ctx)
})

router.get("/stations/:station_id/addVisitor/:visitor_id", async (ctx) => {
    await stationController.addVisitor(ctx)
})

router.get("/stations/:station_id/removeVisitor/", async (ctx) => {
    await stationController.removeVisitor(ctx, config)
})

/////////////
// USERS
/////////////

// POST /login
// router.post('/users/login', async (ctx) => {
//     await userController.login(ctx)
// })
//
// router.get('/users/logout', async (ctx) => {
//     await userController.logout(ctx)
// })


export default router