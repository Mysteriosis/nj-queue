import dotenv from 'dotenv'
dotenv.config({ silent: true })


const config = {
    app: {
        port: process.env.LISTEN_PORT || 3000,
        env:  process.env.ENV || 'development'
    },
    auth: {
        secret: process.env.SESSION_SECRET || "Black metal ist krieg"
    },
    db: {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || '27017',
        name: process.env.DB_NAME || 'test',
        user: process.env.DB_USER || 'root',
        pass: process.env.DB_PASSWORD || ''
    },
    services: {
        mail: {
            host: process.env.MAIL_HOST || 'localhost',
            user: process.env.MAIL_USER || '',
            password: process.env.MAIL_PASSWORD || '',
            port: process.env.MAIL_PORT || 25
        },
        sms: {
            url: process.env.SMS_HOST || 'https://soap.ecall.ch/eCall.asmx',
            user: process.env.SMS_USER || 'user',
            password: process.env.SMS_PASSWORD || 'pwd'
        }
    }
}
  
export default config